ARG DRUPAL_VERSION=latest
FROM docker.io/bitnami/drupal:${DRUPAL_VERSION}

USER 0

## 'install_packages' is a Bitnami-created utility
RUN install_packages vim git npm tar rsync wget curl wait-for-it libxml-xpath-perl

## When you are enabling Shibboleth SSO support, we set the stuff below:
ENV SAMLAUTH_IDP_DIR '/opt/dul/idp'
WORKDIR /opt/dul/idp
RUN wget -O duke-metadata-2-signed.xml https://shib.oit.duke.edu/duke-metadata-2-signed.xml

## Yarn support (optional)
# Get new enough version of Node.js to run NPM correctly
# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
RUN npm install -g yarn cross-env

RUN mkdir -p /.composer/cache/files \
      && chmod -R a+rw /.composer/cache/files

# Create this directory for 'composer' use and grant it full read-write
RUN mkdir -p /.composer/cache/vcs \
  && chmod -R a+rw /.composer/cache

# ALL composer calls will be done via the docker-entrypoint-init.d
# feature supported by Bitnami's chart.

# We need to use a consistent site UUID else configuration
# import will fail. Each instance of the site gets assigned
# a new UUID, yet configs with different UUIDs are incompatible.
# See config/system.site.yml or run:
# drush cget system.site
ENV SYSTEM_SITE_UUID "<your drupal site id>"

WORKDIR /docker-entrypoint-init.d
COPY docker-entrypoint-init.d ./
RUN chmod -R a+x /docker-entrypoint-init.d/*

RUN chmod -R a+rw /opt/bitnami/drupal

ADD uid_entrypoint /bin/

RUN ls -la /

# You'll need to have a 'uid_entrypoint' file in your project root
RUN chmod a+x /bin/uid_entrypoint \
  && touch /etc/passwd \
  && chmod g=u /etc/passwd

ENV DRUPAL_DUL_CONFIG '/opt/dul/config-original'
COPY config/ /opt/dul/config-original/

# Set the COMPOSER env variable, specifying a 
# location that's (more or less) mutable
# ENV COMPOSER /bitnami/drupal/composer.json

WORKDIR /opt/bitnami/drupal
# Ensure the dul-drupal/* repositories are available so 
# future calls to 'composer require|update dul-drupal/...
# won't break.

USER 1001

## You can configure your custom repositories here, 
## OR in a bash (shell) script located in "docker-entrypoint-init.d"

#RUN composer config repositories.dul-drupal/vivid_theme git \
#  https://gitlab.oit.duke.edu/dul-drupal/vivid_theme.git

#RUN composer config repositories.dul-drupal/r2t2_content_type git \
#  https://gitlab.oit.duke.edu/dul-drupal/r2t2_content_type.git

## It may be best to install your modules in a separate bash (shell) 
## script located in 'docker-entrypoint-init.d'
#RUN composer require 'drupal/samlauth'

ENV SHELL /bin/bash
