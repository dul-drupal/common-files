#!/bin/bash

# load logging library
# these files come from Bitnami's Drupal image
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

file_name=$(basename "$0")

if [ -v DRUPAL_SKIP_BOOTSTRAP ] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "[$file_name]: skipping..."
  exit
fi

if [ ! -w /bitnami/drupal/sites/default/settings.php ]; then
  info "[$file_name]: settings file isnt writable. Existing..."
  exit
fi

# Tell Drupal we're running behind a reverse proxy
# 
drupal_conf_set "\$settings['reverse_proxy']" "TRUE" yes
drupal_conf_set "\$settings['reverse_proxy_addresses']" "array(\$_SERVER['REMOTE_ADDR'])" yes
drupal_conf_set "\$settings['reverse_proxy_trusted_headers']" "\\\Symfony\\\Component\\\HttpFoundation\\\Request::HEADER_X_FORWARDED_HOST" yes

## When you need to set your Drupal istance's "private file" directory...
## this directory is useful for images, or other files (from mounted storage, etc).
drupal_conf_set "\$settings['file_private_path']" "/path/to/your/private/files"

# Change config directory from default. See:
# https://www.drupal.org/docs/configuration-management/changing-the-storage-location-of-the-sync-directory
drupal_conf_set "\$settings['config_sync_directory']" "/opt/dul/config"
